# jscrumbbler-tracker

A set of node scripts to track html.

```
npm install jscrumbbler-tracker
```

# CLI usage

## Get page meta track

```
node .\bin\get-page-meta-track \
    --url=https://google.com
```

## Setup client

```
const pageTracking = require('page-tracking')

const config = {
    url: 'https://google.com'
}

pageTracking.init(config)
```

# Example

node .\bin\get-page-meta-track --url=https://google.com

# Testing

This module uses mocha tests. Simply run `npm test`, `npm run test:watch`, or `npm run test:cover`.
