const chai = require('chai')
const sinonChai = require('sinon-chai')
require('../lib/page-tracking').init(require('./client-config'))

chai.use(sinonChai)
