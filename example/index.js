const htmlparser = require('htmlparser2');
const sys = require("util");


var rawHtml = "<html><body><div><a href='cenas' class='xpto'></a><div></div></div></body></html>";
var handler = new htmlparser.DefaultHandler(function (error, dom) {
	if (error)
		console.log(error);
	else
		console.log("ok");
});
var parser = new htmlparser.Parser(handler);
parser.parseComplete(rawHtml);

let treeDeap = 0;

function *processData(data){
  if (!data) { return; }

  for (var i = 0; i< data.length; i++){
    var val = data[i];
    yield { name: val.name, type: val.type, attribs: val.attribs || {} };

    if (val.children) {
      treeDeap ++;
      yield *processData(val.children);
    }
  }
}

var it = processData(handler.dom);
var res = it.next();

const tags = [];

while(!res.done){
  tags.push(res.value);
  res = it.next();
}

var HashTable = {};
var TagsTable = {};

tags.forEach(function(element) {
  // verify the HashTable has an entry for the found tag name
  if ( !(element.name in HashTable) ) {
    // no entry was found so we'll add the entry for this tag name and count it as zero
    HashTable[element.name] = 0;
    TagsTable[element.name] = 0;
  } // end if

  // increment the tag name counter
  HashTable[element.name] ++;
  TagsTable[element.name] += Object.keys(element.attribs).length || 0;
});

console.log(HashTable);
console.log(TagsTable);
console.log(treeDeap);

//sys.puts(sys.inspect(handler.dom, false, null));
