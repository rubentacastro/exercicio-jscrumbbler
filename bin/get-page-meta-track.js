'use strict'
/* eslint-disable no-process-exit */

const yargs = require('yargs');

const logger = require('../lib/logger');
const pageTrackingApi = require('../lib/page-tracking');
const trackPage = require('../lib/commands/simple-track-page');

const args = yargs
  .usage('Usage:\n  $0 [options]')
  .help('help')
  .alias('h', 'help')
  .describe('url', 'The base url of the page')
  .example(`$0 \\\n --url=https://google.com`)
  .argv;

const { url } = args;


pageTrackingApi.init({ url });

const tags = trackPage()
.then(tags => {
  return tags;
})
.catch(err => {
  logger.error('Error:', err)
  process.exit(1)
});
