'use strict'
/* eslint-disable no-console */

const { expect } = require('chai');
const sinon = require('sinon');

const subject = require('../logger');

describe('logger', () => {
  beforeEach(() => {
    sinon.stub(console, 'log');
    sinon.stub(console, 'error');
    sinon.stub(console, 'table');
  })
  afterEach(() => {
    console.log.restore();
    console.table.restore();
    console.error.restore();
  })

  describe('when logging is enabled', () => {
    it('logs info to console', () => {
      subject.info('foo');
      expect(console.log).to.be.called;
    });

    it('logs errors to console', () => {
      subject.error('foo');
      expect(console.error).to.be.called;
    });

    it('logs errors to table', () => {
      subject.table({ 'test': 'foo' });
      expect(console.table).to.be.called;
    });
  });
});

/* eslint-enable no-console */
