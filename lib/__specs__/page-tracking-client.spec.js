'use strict'

const BPromise = require('bluebird');
const { expect } = require('chai');
const request = require('request-promise');
const sinon = require('sinon');

const testConfig = require('../../test/client-config');
const logger = require('../logger');
const PageTrackingClient = require('../page-tracking-client');

describe('page-tracking-client', () => {
  describe('#constructor', () => {
    describe('when config is provided', () => {
      it('initializes the client', () => {
        expect(new PageTrackingClient(testConfig)).to.not.be.null;
      });
    });

    describe('when client config is invalid', () => {
      it('throws when config is null', () => {
        return expect(() => new PageTrackingClient()).to.throw('config is required in PageTrackingClient');
      });

      it('throws when config.host is missing', () => {
        const config = {};
        return expect(() => new PageTrackingClient(config)).to.throw('url is required in PageTrackingClient');
      });

    });
  });

  describe('#get', () => {
    afterEach(() => {
      request.get.restore();
    })

    describe('when request is successful', () => {
      it('returns html data', () => {
        const expected = "<html><body><div><a href='cenas' class='xpto'></a><div></div></div></body></html>";
        sinon.stub(request, 'get').callsFake(() => BPromise.resolve(expected));

        const subject = new PageTrackingClient(testConfig);
        return subject.get('some-url').then(actual => {
          return expect(actual).to.eql(expected);
        })
      })
    })

    describe('when request fails', () => {
      afterEach(() => {
        logger.info.restore();
        logger.error.restore();
      })

      it('returns empty object', () => {
        sinon.stub(request, 'get').callsFake(() => BPromise.reject({ message: 'an error', options: { uri: 'some-url', method: 'GET' } }));
        sinon.stub(logger, 'info');
        sinon.stub(logger, 'error');

        const subject = new PageTrackingClient(testConfig);
        return subject.get('some-url').catch(err => {
          expect(err.message).to.eql('an error');
          expect(logger.error).to.be.calledWith(`Failed to get from 'some-url'. Error: an error`);
          expect(logger.info).to.be.calledWith({ baseUrl: undefined, headers: undefined, method: 'GET', uri: 'some-url' });
        });
      });

      describe('when failure options are not available', () => {
        it('does not log error response info', () => {
          sinon.stub(request, 'get').callsFake(() => BPromise.reject({ message: 'an error', options: null }));
          sinon.stub(logger, 'info');
          sinon.stub(logger, 'error');

          const subject = new PageTrackingClient(testConfig)
          return subject.get('some-url').catch(_ => {
            expect(logger.error).to.be.called;
            expect(logger.info).to.not.be.called;
          });
        });
      });
    });
  });
});
