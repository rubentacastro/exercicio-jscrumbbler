'use strict'

const { expect } = require('chai')

const testConfig = require('../../test/client-config')
const Html = require('../api/html')
const subject = require('../page-tracking')

describe('page-tracking', () => {
  beforeEach(() => {
    subject.close()
  })
  afterEach(() => {
    subject.close()
    subject.init(testConfig)
  })

  describe('#init', () => {
    it('throws an error when already initialized', () => {
      subject.init(testConfig)
      expect(() => subject.init(testConfig)).to.throw('The tracking api client has already been initialized')
    })
  })

  describe('#get *', () => {
    describe('when client is not initialized', () => {
      it('throws errors', () => {
        expect(() => subject.html).to.throw(`The configuration for the api must be set by calling 'init' before making requests`);
      })
    })

    describe('when initialized', () => {
      it('returns api', () => {
        subject.init(testConfig)
        expect(subject.html).to.be.instanceOf(Html)
      })
    })
  })
})
