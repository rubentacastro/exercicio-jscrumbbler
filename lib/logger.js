'use strict'
/* eslint-disable no-console */
require('console.table');

module.exports = {
  info: (...args) => {
      console.log.apply(console, args);
  },
  error: (...args) => {
    console.error.apply(console, args);
  },
  table: (...args) => {
    console.table(args);
  },
}

/* eslint-enable no-console */
