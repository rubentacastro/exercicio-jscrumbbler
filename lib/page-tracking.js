'use strict'

const PageTrackingClient = require('./page-tracking-client');
const Html = require('./api/html');

class PageTracking {
  constructor() {
    this._client = null;
  }

  init(config) {
    if (this._client) {
      throw new Error('The tracking api client has already been initialized');
    }

    this._client = new PageTrackingClient(config);
  }

  close() {
    this._client = null;
    this._html = null;
  }

  _validateClient() {
    if (!this._client) {
      throw new Error(`The configuration for the api must be set by calling 'init' before making requests`)
    }
  }

  get html() {
    this._validateClient();
    if (!this._html) {
      this._html = new Html(this._client);
    }

    return this._html
  }
}

module.exports = new PageTracking()
