'use strict'

const BPromise = require('bluebird');
const { expect } = require('chai');
const sinon = require('sinon');

const createParse = require('../../../test/mocks/parsed');
const subject = require('../get-html-tags').execute;
const pageTracking = require('../../page-tracking');

describe('commands/get-html-tags', () => {

  const parse = createParse();

  describe('when get html parsed', () => {
    it('returns html tags', () => {
      const expected = { html: 1, body: 1, div: 2, a: 1 };

      return subject(parse).then(tags => {
        expect(tags).to.eql(expected);
      });
    });
  });
});
