'use strict'

const BPromise = require('bluebird');
const { expect } = require('chai');
const sinon = require('sinon');

const createParse = require('../../../test/mocks/parsed');
const subject = require('../get-html-attributes').execute;
const pageTracking = require('../../page-tracking');

describe('commands/get-html-attributes', () => {

  const parse = createParse();

  describe('when get html parsed', () => {
    it('returns html attributes by tag', () => {
      const expected = { html: 0, body: 0, div: 0, a: 2 };

      return subject(parse).then(tags => {
        expect(tags).to.eql(expected);
      });
    });
  });
});
