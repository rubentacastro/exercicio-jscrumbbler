'use strict'

const BPromise = require('bluebird');
const { expect } = require('chai');
const sinon = require('sinon');

const createHtml = require('../../../test/mocks/html');
const subject = require('../get-html-parse').execute;
const pageTracking = require('../../page-tracking');

describe('commands/get-html-parse', () => {

  const html = createHtml();

  afterEach(() => {
    pageTracking.html.getPageSource.restore();
  });

  describe('when get html', () => {
    it('returns html parsed', () => {
      sinon.stub(pageTracking.html, 'getPageSource').callsFake(() => BPromise.resolve(html));
      const expected = [{"name":"html","type":"tag","attribs":{}},{"name":"body","type":"tag","attribs":{}},{"name":"div","type":"tag","attribs":{}},{"name":"a","type":"tag","attribs":{"href":"cenas","class":"xpto"}},{"name":"div","type":"tag","attribs":{}}];

      return subject().then(parse => {
        expect(parse).to.eql(expected);
        return expect(pageTracking.html.getPageSource).to.be.calledWith();
      });
    });
  });
});
