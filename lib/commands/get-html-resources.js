'use strict'

const logger = require('../logger');
const pageTrackingApi = require('../page-tracking');
const BPromise = require('bluebird');
const url = require("url");

const getHtmlResources = (tags) => {
  logger.info(`* Downloaded resource types (image, video, etc) and hosts from where they were downloaded:`);

  var result = {images: [], videos: []};

  tags.forEach(function(element) {

    // verify if element is an image resource
    if(element.name == 'img') {
      result.images.push(`Src: ${ element.attribs.src }, Host: ${ url.parse(element.attribs.src).hostname || "local" }`);
    }

    // verify if element is an image resource
    if(element.name == 'audio'
      || element.name == 'source'
      || element.name == 'track'
      || element.name == 'video'
    )
    {
      result.videos.push(`Src: ${ element.attribs.src }, Host: ${ url.parse(element.attribs.src).hostname }`);
    }
  });

  // return promisse with result
  return new BPromise((resolve, reject) => {
    resolve(result);
  });
}

module.exports.execute = getHtmlResources;
