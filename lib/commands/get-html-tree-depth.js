'use strict'

const logger = require('../logger');
const BPromise = require('bluebird');

const getHtmlTreeDepth = (tags) => {

  logger.info(`* The depth of the tree:`);

  var result = {};

  tags.forEach(function(element) {
    // verify the result has an entry for the found tag name
    if ( !(element.name in result) ) {
      // no entry was found so we'll add the entry for this tag name and count it as zero
      result[element.name] = 0;
    } // end if

    // increment the tag name counter
    result[element.name] ++;
  });

  // return promisse with result
  return new BPromise((resolve, reject) => {
    resolve(result);
  });
}

module.exports.execute = getHtmlTreeDepth;
