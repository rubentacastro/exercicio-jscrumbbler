'use strict'

const logger = require('../logger');
const getHtmlParse = require('./get-html-parse');
const getHtmlTags = require('./get-html-tags');
const getHtmlAttributes = require('./get-html-attributes');
const getHtmlResources = require('./get-html-resources');
const url = require('url');

const simpleTrackPage = (params) => {

  let htmlParseObj;

  return getHtmlParse.execute()
  .then(htmlParse => {
    htmlParseObj = htmlParse;
    return getHtmlTags.execute(htmlParse);
  })

  .then(tags => {
    logger.table(tags);
    return getHtmlAttributes.execute(htmlParseObj);
  })

  .then(attribs => {
    logger.table(attribs);
    return getHtmlResources.execute(htmlParseObj);
  })
  .then(resources => {
    logger.info("   - Images:");
    logger.table(resources.images);

    logger.info("   - Videos:");
    logger.table(resources.videos);

    return resources;
  })
}

module.exports = simpleTrackPage;
