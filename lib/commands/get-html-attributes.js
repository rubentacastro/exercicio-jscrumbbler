'use strict'

const logger = require('../logger');
const pageTrackingApi = require('../page-tracking');
const BPromise = require('bluebird');

const getHtmlAttributesByTag = (tags) => {
  //logger.info(`Preparing deployment process '${deploymentProcessId}' with package verion '${version}'`)
  return pageTrackingApi.html.getPageSource()
  .then(html => {
    logger.info(`* Attribute count by HTML tag:`);

    var result = {};

    tags.forEach(function(element) {
      // verify the result has an entry for the found tag name
      if ( !(element.name in result) ) {
        // no entry was found so we'll add the entry for this tag name and count it as zero
        result[element.name] = 0;
      } // end if

      // increment attributes
      result[element.name] += Object.keys(element.attribs).length || 0;
    });

    // return promisse with result
    return new BPromise((resolve, reject) => {
      resolve(result);
    });
  });
}

module.exports.execute = getHtmlAttributesByTag;
