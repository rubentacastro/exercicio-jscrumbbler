'use strict'

const logger = require('../logger');
const pageTrackingApi = require('../page-tracking');
const BPromise = require('bluebird');
const htmlparser = require('htmlparser2');
const sys = require("util");

let treeDeap = 0;

function *processData(data){
  if (!data) { return; }

  for (var i = 0; i< data.length; i++){
    var val = data[i];
    yield { name: val.name, type: val.type, attribs: val.attribs || {} };

    if (val.children) {
      treeDeap ++;
      yield *processData(val.children);
    }
  }
}

const getHtmlParsed = () => {
  logger.info(`* Parse html from page`);

  return pageTrackingApi.html.getPageSource()
  .then(html => {

    var handler = new htmlparser.DefaultHandler(function (error, dom) {
      if (error)
        console.log(error);
      else
        console.log("ok");
    }, { verbose: false, ignoreWhitespace: true });

    var parser = new htmlparser.Parser(handler);
    parser.parseComplete(html);

    return new BPromise((resolve, reject) => {

      var it = processData(handler.dom);
      var res = it.next();

      const tags = [];

      while(!res.done){
        tags.push(res.value);
        res = it.next();
      }

      resolve(tags);
    });
  });
}

module.exports.execute = getHtmlParsed;
